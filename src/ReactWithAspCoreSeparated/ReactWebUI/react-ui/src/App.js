import { FetchData } from './components/FetchData';
import logo from './logo.svg';
import './App.css';

function App() {
    return (
        <div>
            <FetchData/>
        </div>
  );
}

export default App;
